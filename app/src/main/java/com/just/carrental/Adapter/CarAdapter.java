package com.just.carrental.Adapter;
import com.just.carrental.RequestCar;
import com.squareup.picasso.Picasso;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.just.carrental.CarsFragment;
import com.just.carrental.Config;
import com.just.carrental.HomeActivity;

import com.just.carrental.Model.cars;
import com.just.carrental.R;

import java.util.ArrayList;

public class CarAdapter extends RecyclerView.Adapter<CarAdapter.MyViewHolder> {
    private LayoutInflater inflater;
    private ArrayList<cars> list;
    Context context;

    private static final String TAG = "CarAdapter";
    public CarAdapter(Context ctx, ArrayList<cars> list){
        context = ctx;
        inflater = LayoutInflater.from(ctx);
        this.list = list;

    }

    @Override
    public CarAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.zrow_view_car, parent, false);
        CarAdapter.MyViewHolder holder = new CarAdapter.MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(CarAdapter.MyViewHolder holder, final int position) {
        cars obj = list.get(position);
        holder.car_info.setText(list.get(position).getC_type() + " " + list.get(position).getTitle() + "-" + list.get(position).getModel());
        holder.color.setText(list.get(position).getColor());
        holder.s_num.setText(list.get(position).getS_num());
        holder.car_size.setText(list.get(position).getCar_size());
        holder.price.setText(list.get(position).getPrice()+" JOD");
        holder.car_status.setText(list.get(position).getStatus());
        Picasso.get().load(Config.BASE_IMAGE+obj.getCar_image()).into(holder.car_image);

        if(holder.car_status.getText().toString().equals("0")){
            //holder.car_status.setText("Unavailable");
           // holder.car_status.setVisibility(View.VISIBLE);
            holder.book_now.setEnabled(false);
            holder.book_now.setText("Unavailable");

        }else{
            holder.book_now.setEnabled(true);
            holder.book_now.setText("Book Now");
        }


        //holder.car_image.setText(list.get(position).getCar_image());
            holder.book_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "onClick: " + "Selected ID : " + list.get(position).getId());
                //Toast.makeText(context, "Selected  ID: " + list.get(position).getId(), Toast.LENGTH_LONG).show();


                Config.SelectedCar = list.get(position).getId();


                ((HomeActivity) context).setFragment(new RequestCar(), false);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView car_info, color, car_size, price, s_num, car_status;
        ImageView car_image;
        Button book_now;
        RelativeLayout relativeLayout;



        public MyViewHolder(View itemView) {
            super(itemView);
            car_info = itemView.findViewById(R.id.car_info);
            color = itemView.findViewById(R.id.color);
            car_size = itemView.findViewById(R.id.size);
            price = itemView.findViewById(R.id.price);
            s_num = itemView.findViewById(R.id.s_num);
            car_image = itemView.findViewById(R.id.car_image);
            car_status=itemView.findViewById(R.id.txt_status);
            book_now=itemView.findViewById(R.id.btn_book_now);
            relativeLayout = itemView.findViewById(R.id.relate);

        }

    }
}