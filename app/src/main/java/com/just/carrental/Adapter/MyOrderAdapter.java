package com.just.carrental.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.just.carrental.CarsFragment;
import com.just.carrental.Config;
import com.just.carrental.HomeActivity;
import com.just.carrental.Model.MyOrderModel;
import com.just.carrental.Model.Office;
import com.just.carrental.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;



public class MyOrderAdapter extends RecyclerView.Adapter<MyOrderAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<MyOrderModel> list;


    Context context;

    private static final String TAG = "MyOrderAdapter";

    public MyOrderAdapter(Context ctx, ArrayList<MyOrderModel> list){
        context = ctx;
        inflater = LayoutInflater.from(ctx);
        this.list = list;

    }

    @Override
    public MyOrderAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.zrow_my_order, parent, false);
        MyOrderAdapter.MyViewHolder holder = new MyOrderAdapter.MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyOrderAdapter.MyViewHolder holder, final int position) {

        MyOrderModel obj = list.get(position);


        holder.txt_car.setText(obj.getCar_type()+" "+obj.getCar_title()+" "+obj.getCar_model());
        holder.txt_car_number.setText(obj.getCar_number());
        holder.txt_sdate.setText(obj.getStart_date());
        holder.txt_edate.setText(obj.getEnd_date());
        Picasso.get().load(Config.BASE_IMAGE+obj.getImg()).into(holder.imageView);



//        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.e(TAG, "onClick: "+"Selected ID : "+list.get(position).getId() );
//                Toast.makeText(context, "Selected ID : "+list.get(position).getId() , Toast.LENGTH_LONG).show();
//
//
//                Config.Selected_Office = list.get(position).getId();
//
//
//                ((HomeActivity) context).setFragment(new CarsFragment(),false);
//
//
//
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView txt_car,txt_car_number,txt_sdate,txt_edate;
        RelativeLayout relativeLayout;
        ImageView imageView;



        public MyViewHolder(View itemView) {
            super(itemView);
            txt_car =  itemView.findViewById(R.id.txt_car);
            txt_car_number =itemView.findViewById(R.id.txt_car_number);
            txt_sdate =itemView.findViewById(R.id.txt_sdate);
            txt_edate =itemView.findViewById(R.id.txt_edate);
            imageView =itemView.findViewById(R.id.img_car);



            relativeLayout = itemView.findViewById(R.id.relate);

        }

    }
}
