package com.just.carrental.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.just.carrental.CarsFragment;
import com.just.carrental.Config;
import com.just.carrental.HomeActivity;
import com.just.carrental.Model.Office;
import com.just.carrental.R;

import java.util.ArrayList;

public class OfficeAdapter extends RecyclerView.Adapter<OfficeAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<Office> list;


    Context context;

    private static final String TAG = "OfficeAdapter";

    public OfficeAdapter(Context ctx, ArrayList<Office> list){
        context = ctx;
        inflater = LayoutInflater.from(ctx);
        this.list = list;

    }

    @Override
    public OfficeAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.zrow_office, parent, false);
        OfficeAdapter.MyViewHolder holder = new OfficeAdapter.MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(OfficeAdapter.MyViewHolder holder, final int position) {

        holder.name.setText(list.get(position).getFname()+" "+list.get(position).getLname());
        holder.phone.setText(list.get(position).getPhone());
        holder.email.setText(list.get(position).getEmail());

        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "onClick: "+"Selected ID : "+list.get(position).getId() );
                //Toast.makeText(context, "Selected ID : "+list.get(position).getId() , Toast.LENGTH_LONG).show();


                Config.Selected_Office = list.get(position).getId();


                ((HomeActivity) context).setFragment(new CarsFragment(),false);



            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView name,email,phone;
        RelativeLayout relativeLayout;



        public MyViewHolder(View itemView) {
            super(itemView);
            name =  itemView.findViewById(R.id.txt_name);
            phone =itemView.findViewById(R.id.txt_phone);
            email =itemView.findViewById(R.id.txt_email);
            relativeLayout = itemView.findViewById(R.id.relate);
        }

    }
}
