package com.just.carrental;

import android.app.FragmentManager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.just.carrental.Adapter.CarAdapter;
import com.just.carrental.Adapter.CarAdapter;
import com.just.carrental.Model.Office;
import com.just.carrental.Model.cars;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CarsFragment extends Fragment {
    View v;
    RecyclerView list;
    ArrayList dataList;
    String uid;
    Context context;
    TextView car_status;


    private static final String TAG = "CarsFragment";

    public CarsFragment() {

    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v =  inflater.inflate(R.layout.fragment_cars, container, false);

        init();



        return  v;

    }


    public void init(){
        uid = Config.Selected_Office;
        Log.e(TAG, "init: Selected_ID -> "+uid);
        //Toast.makeText(getContext(), Config.Selected_Office, Toast.LENGTH_SHORT).show();
        dataList = new ArrayList<cars>();

        list = v.findViewById(R.id.list);
        getData();
    }

    public void getData(){

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.VIEW_CAR, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    Log.e(TAG, "onResponse: "+response);
                    JSONObject base = new JSONObject(response);
                    JSONObject result = base.getJSONObject("result");
                    String status = result.getString("status");
                    String message = result.getString("message");

                    if (status.equals("0")){
                        getActivity().getSupportFragmentManager().popBackStack();
                        //((HomeActivity) context).setFragment(new homeFragment(),false);

                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    }else{

                        JSONArray data = base.getJSONArray("data");

                        for (int i = 0;i<data.length();i++){
                            JSONObject object = data.getJSONObject(i);
                            cars o = new cars();
                            o.setId(object.getString("id"));
                            o.setUid(object.getString("uid"));
                            o.setC_type(object.getString("c_type"));
                            o.setTitle(object.getString("title"));
                            o.setModel(object.getString("model"));
                            o.setColor(object.getString("color"));
                            o.setCar_size(object.getString("car_size"));
                            o.setS_num(object.getString("s_num"));
                            o.setStatus(object.getString("status"));
                            o.setPrice(object.getString("price"));
                            o.setCar_image(object.getString("car_image"));
                            o.setStatus(object.getString("status"));
                            dataList.add(o);
                        }
                        CarAdapter carAdapter = new CarAdapter(((HomeActivity)getContext()),dataList);
                        list.setLayoutManager(new LinearLayoutManager(getContext()));
                        list.setAdapter(carAdapter);




                    }




                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse: "+error );
            }
        }){
            @Nullable
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                uid = Config.Selected_Office;
                Map map = new HashMap<String,String>();

                map.put("type","view_cars_by_id");
                map.put("id",uid);
                return map;
            }
        };


        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);


    }


}