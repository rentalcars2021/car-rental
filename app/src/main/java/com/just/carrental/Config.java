package com.just.carrental;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.android.material.textfield.TextInputLayout;

public class Config {

    public static String BASE_IMAGE = "http://booking.youvuz.com/";
    public static String PREF = "BOOKING";
    public static String USER = "http://booking.youvuz.com/ws/users.php";
    public static String REQUESTS = "http://booking.youvuz.com/ws/requests.php";
    public static String VIEW_CAR = "http://booking.youvuz.com/ws/cars.php";

    public static String Selected_Office = "";
    public static String SelectedCar = "";









    public static void saveValue(Context c, String key, String value){
        SharedPreferences pref = c.getSharedPreferences(PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();


        editor.putString(key, value);  // Saving string

        editor.apply(); // commit changes

    }


    public static String getValue(Context c,String key){
        String val = "";
        SharedPreferences pref = c.getSharedPreferences(PREF, Context.MODE_PRIVATE);
        val=pref.getString(key, "");

        return val;
    }

    public static void deleteValues(Context c) {
        SharedPreferences pref = c.getSharedPreferences(PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        editor.clear();
        editor.apply(); // commit changes
    }

    public static boolean isMoreThan3(TextInputLayout txt, String error){
        if (txt.getEditText().getText().toString().length()<3){
            txt.setErrorEnabled(true);
            txt.setError(error);
            return false;
        }else{
            txt.setErrorEnabled(false);
            return true;
        }
    }



    public static boolean isValidEmailAddress(TextInputLayout txt, String error) {
        String email = txt.getEditText().getText().toString();
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        if (!email.matches(regex)){
            txt.setErrorEnabled(true);
            txt.setError(error);
            return false;
        }else{
            txt.setErrorEnabled(false);
            return true;
        }
    }



}
