package com.just.carrental;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.navigation.NavigationView;

import androidx.annotation.ColorRes;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class HomeActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;


    NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    DrawerLayout drawerLayout_main;
    androidx.appcompat.widget.Toolbar toolbar;
    ImageView btn_open_menu ,btn_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        drawerLayout = findViewById(R.id.drawerLayout_main);
        navigationView = findViewById(R.id.nva_menu);
        toolbar = findViewById(R.id.toolbar);


        View headerView = navigationView.getHeaderView(0);
        TextView navUsername = (TextView) headerView.findViewById(R.id.navUsername);
        navUsername.setText(Config.getValue(getApplicationContext(),"fname")+" "+Config.getValue(getApplicationContext(),"lname"));


        TextView email = (TextView) headerView.findViewById(R.id.navEmail);
        email.setText(Config.getValue(getApplicationContext(),"email"));

        actionBarDrawerToggle=new ActionBarDrawerToggle(this,drawerLayout,R.string.open,R.string.close);
        drawerLayout_main = (DrawerLayout)findViewById(R.id.drawerLayout_main);



        rebuildNav(navigationView);
        btn_open_menu = findViewById(R.id.btn_open_menu);
        btn_back = findViewById(R.id.btn_back);

        btn_open_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMenu();
            }
        });


        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backFragment();
            }
        });





        resetAllMenuItemsTextColor(navigationView);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                resetAllMenuItemsTextColor(navigationView);
                setTextColorForMenuItem(item, R.color.light);
                switch (item.getItemId()){
                    case R.id.nav_home:
                        setFragment(new homeFragment(),false);
                        break;
                    case R.id.nav_order:
                        setFragment(new MyOrder(),false);
                        break;
                    case R.id.nav_nearby:
                        setFragment(new NearbyLocation(),false);
                        break;
                    case R.id.nav_logout:
                        Config.deleteValues(getApplicationContext());
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        finish();
                        break;




                }
                rebuildNav(navigationView);
                return false;
            }
        });


        setFragment(new homeFragment(),false);





    }

    public void openMenu(){
        drawerLayout_main.openDrawer(GravityCompat.START);
    }




    //    move between fragment and remove the cache from th mobile
    public void setFragment(Fragment fragment, boolean back) {
        rebuildNav(navigationView);
        FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
        tx.replace( R.id.frameLayout, fragment ).addToBackStack( "tag" ).commit();



        Log.e("fragment ID : ",fragment.toString().contains("Home")+"");


    }

    private void setTextColorForMenuItem(MenuItem menuItem, @ColorRes int color) {
        SpannableString spanString = new SpannableString(menuItem.getTitle().toString());
        spanString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, color)), 0, spanString.length(), 0);
        menuItem.setTitle(spanString);

    }


    private void rebuildNav(NavigationView navigationView){



        resetAllMenuItemsTextColor(navigationView);
        drawerLayout_main.closeDrawers();
    }

    private void resetAllMenuItemsTextColor(NavigationView navigationView) {
        navigationView.setItemIconTintList(null);
        for (int i = 0; i < navigationView.getMenu().size(); i++){
            MenuItem menuItem = navigationView.getMenu().getItem(i);
            String mTitle = menuItem.getTitle().toString();
            SpannableString spanString = new SpannableString(menuItem.getTitle().toString());
            spanString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.dark)), 0, spanString.length(), 0);
            menuItem.setTitle(spanString);
        }

    }

    public void backFragment() {

        getSupportFragmentManager().popBackStack();


    }

    public void removeFragment(){
        androidx.fragment.app.FragmentManager fm = getSupportFragmentManager();
        for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }

    }

    boolean doubleBackToExitPressedOnce = false;
    @Override
    public void onBackPressed() {
        closeMenu();

        //Checking for fragment count on backstack
        if (getSupportFragmentManager().getBackStackEntryCount() != 0) {

            getSupportFragmentManager().popBackStack();

        } else if (!doubleBackToExitPressedOnce) {
            this.doubleBackToExitPressedOnce = true;
            getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            Toast.makeText(this, "Please click BACK again to exit.", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        } else {
            super.onBackPressed();
            return;
        }

    }
    public void closeMenu(){
        drawerLayout_main.closeDrawers();
    }








}