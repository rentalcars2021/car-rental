package com.just.carrental;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputLayout;
import com.just.carrental.Config;
import com.just.carrental.R;

import org.json.JSONException;
import org.json.JSONObject;

    import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public static boolean mMapIsTouched = false;

    TextInputLayout email,password;
    Button login;
    CheckedTextView signup ;

    private static final String TAG = "MainActivity";





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (!Config.getValue(getApplicationContext(),"id").equals("")){
            Intent intent = new Intent(getApplicationContext(),HomeActivity.class);
            startActivity(intent);
            finish();
        }
        email = findViewById(R.id.txt_femail);
        Intent i = getIntent();
        String email1 = i.getStringExtra("email");
        email.getEditText().setText(email1);



        init();





    }


    public void init(){

        email = findViewById(R.id.txt_femail);
        password = findViewById(R.id.txt_fpassword);


        password.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Config.isMoreThan3(password,"Password Not Correct");
            }
        });





        email.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Config.isValidEmailAddress(email,"Email Not Correct");

            }
        });



        login = findViewById(R.id.login);
        login.setOnClickListener(this);
        signup=findViewById(R.id.checked_txt_signUp);
        signup.setOnClickListener(this);

    }








    public void sendRequest(){
        Boolean flag = true;

        if (!Config.isValidEmailAddress(email,"Email Not Correct")){
            flag = false;

        }


        if (!Config.isMoreThan3(password,"Password Not Correct")){
            flag = false;
        }

        if (flag){



            StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.USER, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.e(TAG, "onResponse: "+response );
                    try {
                        JSONObject base = new JSONObject(response);
                        JSONObject result = base.getJSONObject("result");
                        String status = result.getString("status");
                        String message = result.getString("message");

                        if (status.equals("0")){
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        }else{

                            JSONObject data = base.getJSONObject("data");
                            Config.saveValue(getApplicationContext(),"id",data.getString("id"));
                            Config.saveValue(getApplicationContext(),"fname",data.getString("fname"));
                            Config.saveValue(getApplicationContext(),"email",data.getString("email"));
                            Config.saveValue(getApplicationContext(),"password",data.getString("password"));
                            Config.saveValue(getApplicationContext(),"mobile",data.getString("mobile"));
                            Config.saveValue(getApplicationContext(),"lname",data.getString("lname"));
                            Config.saveValue(getApplicationContext(),"u_type",data.getString("u_type"));
                            Config.saveValue(getApplicationContext(),"status",data.getString("status"));

                            if (data.getString("u_type").equals("1") || data.getString("u_type").equals("2") ){
                            Toast.makeText(getApplicationContext(), "Please use the website to login", Toast.LENGTH_SHORT).show();

                            }else{
                                Intent intent = new Intent(getApplicationContext(),HomeActivity.class);
                                startActivity(intent);
                                finish();
                            }



                        }

//                        Toast.makeText(getApplicationContext(), status, Toast.LENGTH_SHORT).show();



                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "onErrorResponse: "+error );
                }
            }){
                @Nullable
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map map = new HashMap<String,String>();
                    map.put("type","login");
                    map.put("email",email.getEditText().getText().toString().trim());
                    map.put("password",password.getEditText().getText().toString().trim());
                    return map;

                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);





        }else {
            Toast.makeText(this, "Please Enter valid Information", Toast.LENGTH_LONG).show();
        }




    }




    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.login:
                sendRequest();
                break;
            case R.id.checked_txt_signUp:
                Intent intent = new Intent(MainActivity.this,createAccount.class);
                    startActivity(intent);

                    break;

        }
    }
}