package com.just.carrental;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.just.carrental.Adapter.MyOrderAdapter;
import com.just.carrental.Adapter.OfficeAdapter;
import com.just.carrental.Model.MyOrderModel;
import com.just.carrental.Model.Office;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyOrder extends Fragment {

    private static final String TAG = "MyOrder";

    public MyOrder() {
        // Required empty public constructor
    }

    View v;
    RecyclerView list;
    ArrayList dataList;

    String uid;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_my_order, container, false);

        init();
        return v;

    }

    public void init(){
        uid = Config.getValue(getContext(),"id");
        Log.e(TAG, "init: user id "+uid );
        dataList = new ArrayList<MyOrderModel>();

        list = v.findViewById(R.id.list);
        getData();



    }



    public void getData(){

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.REQUESTS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    Log.e(TAG, "onResponse: "+response );
                    JSONObject base = new JSONObject(response);
                    JSONObject result = base.getJSONObject("result");
                    String status = result.getString("status");
                    String message = result.getString("message");

                    if (status.equals("0")){
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    }else{

                        JSONArray data = base.getJSONArray("data");

                        for (int i = 0;i<data.length();i++){
                            JSONObject object = data.getJSONObject(i);
                            MyOrderModel o = new MyOrderModel();
                            o.setId(object.getString("request_id"));
                            o.setCar_title(object.getString("car_title"));
                            o.setCar_type(object.getString("car_type"));
                            o.setCar_number(object.getString("car_number"));
                            o.setCar_model(object.getString("car_model"));
                            o.setImg(object.getString("img"));
                            o.setStart_date(object.getString("start_date"));
                            o.setEnd_date(object.getString("end_date"));

                            dataList.add(o);
                        }

                        MyOrderAdapter myOrderAdapter = new MyOrderAdapter(((HomeActivity) getContext()), dataList);
                        list.setLayoutManager(new LinearLayoutManager(getContext()));
                        list.setAdapter(myOrderAdapter);
                    }




                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse: "+error );
            }
        }){
            @Nullable
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                uid = Config.getValue(getContext(),"id");
                Map map = new HashMap<String,String>();

                map.put("type","view_car_requested_user");
                map.put("id",uid);
                return map;
            }
        };


        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);















    }






}