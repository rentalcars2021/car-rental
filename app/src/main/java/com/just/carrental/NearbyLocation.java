package com.just.carrental;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.content.pm.PackageManager;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.just.carrental.Adapter.OfficeAdapter;
import com.just.carrental.Model.Office;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class NearbyLocation extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {


    View v;
    private static final String TAG = "NearbyLocation";


    private GpsTracker gpsTracker;
    private Double tvLatitude,tvLongitude;
    private FragmentActivity myContext;
    ArrayList <Office>dataList;


    GoogleMap mMap;
    SupportMapFragment mapFrag;

    ArrayList<LatLng>arrayList = new ArrayList<LatLng>();
    LatLng currentLocation = new LatLng(32.537449, 35.855372);

    private Context context;
    Bundle savedInstanceStates;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v =  inflater.inflate(R.layout.fragment_nearby_location, container, false);


        savedInstanceStates = savedInstanceState;
        this.context = container.getContext();

        init();

        return v;
    }




    public void init(){
        dataList = new ArrayList<Office>();

        try {
            if (ContextCompat.checkSelfPermission(v.getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {
                ActivityCompat.requestPermissions( ((HomeActivity) getContext()), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 101);
            }else {
                getLocation(v);

                mapFrag = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.map);
                currentLocation = new LatLng(tvLatitude,tvLongitude);
                getData();




            }
        } catch (Exception e){
            e.printStackTrace();
        }








    }




    public void getData(){

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.USER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    Log.e(TAG, "onResponse: "+response );
                    JSONObject base = new JSONObject(response);
                    JSONObject result = base.getJSONObject("result");
                    String status = result.getString("status");
                    String message = result.getString("message");

                    if (status.equals("0")){
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    }else{

                        JSONArray data = base.getJSONArray("data");

                        for (int i = 0;i<data.length();i++){
                            JSONObject object = data.getJSONObject(i);
                            Office o = new Office();
                            o.setId(object.getString("id"));
                            o.setFname(object.getString("fname"));
                            o.setLname(object.getString("lname"));
                            o.setEmail(object.getString("email"));
                            o.setPhone(object.getString("mobile"));
                            o.setLat(Double.parseDouble(object.getString("lat")));
                            o.setLang(Double.parseDouble(object.getString("lang")));




                            dataList.add(o);
                        }

                        generateMapView();



                    }




                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse: "+error );
            }
        }){
            @Nullable
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map map = new HashMap<String,String>();

                map.put("type","view_all_office");
                return map;
            }
        };


        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);

    }


    public void generateMapView() {
        if (mMap == null) {
            Log.e("MapView :", "null");
            if (getActivity() != null) {
                mapFrag.onCreate(savedInstanceStates);
                mapFrag.getMapAsync(this);

            }
        } else {
            Log.e("MapView :", "not null");
        }

    }

    private void addMarker(GoogleMap map, double lat, double lon,
                           String title, String snippet) {
        map.addMarker(new MarkerOptions().position(new LatLng(lat, lon))
                .title(title)
                .snippet(snippet)
                .draggable(true));


    }


    public void getLocation(View view){
        gpsTracker = new GpsTracker(v.getContext());
        if(gpsTracker.canGetLocation()){
            double latitude = gpsTracker.getLatitude();
            double longitude = gpsTracker.getLongitude();
            tvLatitude = latitude;
            tvLongitude=longitude;
            Log.e(TAG, "getLocation: "+tvLatitude+","+tvLongitude );
            String d = distance(31.725315, 35.982502,31.790580, 35.925601,'K' )+"";
            Log.e(TAG, "getLocation Distance: "+d+" KM" );



        }else{
            gpsTracker.showSettingsAlert();
        }
    }



    private double distance(double lat1, double lon1, double lat2, double lon2, char unit) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        if (unit == 'K') {
            dist = dist * 1.609344;
        } else if (unit == 'N') {
            dist = dist * 0.8684;
        }
        return (dist);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::  This function converts decimal degrees to radians             :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::  This function converts radians to decimal degrees             :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
//

        mMap.setOnMarkerClickListener(this);
        for (int i = 0;i<dataList.size();i++){
            Office  o = new Office();
            o = dataList.get(i);

            if (distance(tvLatitude,tvLongitude,o.getLat(),o.getLang(),'K') < 1000){
                LatLng latLng = new LatLng(o.getLat(),o.getLang() );
                mMap.addMarker(new MarkerOptions().position(latLng).title(o.getFname()+" "+o.getLname()+" ( "+o.getPhone()+" )").snippet(o.getId()));
            }





        }






        mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLocation));

         CameraPosition.Builder camBuilder = CameraPosition.builder();
         camBuilder.bearing(45);
         camBuilder.tilt(30);
         camBuilder.target(currentLocation);
         camBuilder.zoom(9);

         CameraPosition cp = camBuilder.build();
         mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cp));

         mapFrag.onStart();

    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        String id =  marker.getSnippet();
        Config.Selected_Office = id;
        ((HomeActivity) context).setFragment(new CarsFragment(),false);

        return false;
    }
}