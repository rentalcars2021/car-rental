package com.just.carrental;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.just.carrental.Adapter.OfficeAdapter;
import com.just.carrental.Model.Office;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public class RequestCar extends Fragment implements View.OnClickListener {

    View v ;
    Button btnDatePicker, btnTimePicker;
    EditText txtDate, txtTime;


    Button btneDatePicker, btneTimePicker,btn_confirm;
    EditText txteDate, txteTime;
    String lat,lang = "";

    private int mYear, mMonth, mDay, mHour, mMinute;


    private int meYear, meMonth, meDay, meHour, meMinute;

    private GpsTracker gpsTracker;


    public void RequestCar(){

    }

    private static final String TAG = "RequestCar";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v =  inflater.inflate(R.layout.fragment_request_car, container, false);



        init();


        return v;
    }


    public void init(){

        btnDatePicker=v.findViewById(R.id.btn_date);
        btnTimePicker=v.findViewById(R.id.btn_time);
        txtDate=v.findViewById(R.id.in_date);
        txtTime=v.findViewById(R.id.in_time);

        btnDatePicker.setOnClickListener(this);
        btnTimePicker.setOnClickListener(this);


        btneDatePicker=v.findViewById(R.id.btn_out_date);
        btneTimePicker=v.findViewById(R.id.btn_out_time);
        txteDate=v.findViewById(R.id.out_date);
        txteTime=v.findViewById(R.id.out_time);

        btn_confirm=v.findViewById(R.id.btn_confirm);

        btn_confirm.setOnClickListener(this);
        btneDatePicker.setOnClickListener(this);
        btneTimePicker.setOnClickListener(this);



        try {
            if (ContextCompat.checkSelfPermission(v.getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {
                ActivityCompat.requestPermissions( ((HomeActivity) getContext()), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 101);
            }else {
                getLocation(v);


            }
        } catch (Exception e){
            e.printStackTrace();
        }


    }



    public void getLocation(View view){
        gpsTracker = new GpsTracker(v.getContext());
        if(gpsTracker.canGetLocation()){
            double latitude = gpsTracker.getLatitude();
            double longitude = gpsTracker.getLongitude();
            lat = latitude+"";
            lang =longitude+"";
            Log.e(TAG, "getLocation: "+lat+","+lang );
//            String d = distance(31.725315, 35.982502,31.790580, 35.925601,'K' )+"";
//            Log.e(TAG, "getLocation Distance: "+d+" KM" );



        }else{
            gpsTracker.showSettingsAlert();
        }
    }



    public void sendData(){

        Toast.makeText(v.getContext() , "Data Send", Toast.LENGTH_SHORT).show();




//



        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.REQUESTS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject base = new JSONObject(response);
                    JSONObject result = base.getJSONObject("result");
                    String status = result.getString("status");
                    String message = result.getString("message");


                    if (status == "0"){
                        Toast.makeText(v.getContext()  , message, Toast.LENGTH_LONG).show();
                    }else{
                        Toast.makeText(v.getContext()  , message, Toast.LENGTH_LONG).show();
                        btn_confirm.setEnabled(false);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse: "+error.getMessage() );
            }
        }){
            @Nullable
            @org.jetbrains.annotations.Nullable
            @Override
            protected Map<String, String> getParams() {


                String start = txtDate.getText().toString() + " "+ txtTime.getText().toString();
                String end = txteDate.getText().toString() + " "+ txteTime.getText().toString();
                String uid = Config.getValue(v.getContext(),"id");
                String office_id = Config.Selected_Office;
                String car_id = Config.SelectedCar;


                Map map = new HashMap<String,String>();
                map.put("type","book_car");
                map.put("car_id",car_id);
                map.put("user_book_id",uid);
                map.put("office_id",office_id);
                map.put("book_start_date",start);
                map.put("book_end_date",end);
                map.put("status","0");
                map.put("lat",lat);
                map.put("lang",lang);

                return map;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(v.getContext());
        requestQueue.add(stringRequest);









    }


    public void confirmBook(){

        Boolean flag = true;
        if (txtDate.getText().toString().equals("")){
            txtDate.setError("Start Date Not Correct");
            flag = false;
        }else {
            txtDate.setError(null);
        }

        if (txtTime.getText().toString().equals("")){
            txtTime.setError("Start Time Not Correct");
            flag = false;
        }else {
            txtTime.setError(null);
        }


        if (txteDate.getText().toString().equals("")){
            txteDate.setError("End Date Not Correct");
            flag = false;
        }else {
            txteDate.setError(null);
        }

        if (txteTime.getText().toString().equals("")){
            txteTime.setError("End Time Not Correct");
            flag = false;
        }else {
            txteTime.setError(null);
        }

        if (flag == true){
            sendData();
        }

    }



    @Override
    public void onClick(View v) {

        if (v == btnDatePicker) {

            // Get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(v.getContext(),
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            txtDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }
        if (v == btnTimePicker) {

            // Get Current Time
            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            // Launch Time Picker Dialog
            TimePickerDialog timePickerDialog = new TimePickerDialog(v.getContext(),
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {

                            txtTime.setText(hourOfDay + ":" + minute);
                        }
                    }, mHour, mMinute, false);
            timePickerDialog.show();
        }
        //// end
        if (v == btneDatePicker) {

            // Get Current Date
            final Calendar c = Calendar.getInstance();
            meYear = c.get(Calendar.YEAR);
            meMonth = c.get(Calendar.MONTH);
            meDay = c.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(v.getContext(),
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            txteDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                        }
                    }, meYear, meMonth, meDay);
            datePickerDialog.show();
        }
        if (v == btneTimePicker) {

            // Get Current Time
            final Calendar c = Calendar.getInstance();
            meHour = c.get(Calendar.HOUR_OF_DAY);
            meMinute = c.get(Calendar.MINUTE);

            // Launch Time Picker Dialog
            TimePickerDialog timePickerDialog = new TimePickerDialog(v.getContext(),
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {

                            txteTime.setText(hourOfDay + ":" + minute);
                        }
                    }, meHour, meMinute, false);
            timePickerDialog.show();
        }

        if (v == btn_confirm) {
            confirmBook();
        }



    }
}
