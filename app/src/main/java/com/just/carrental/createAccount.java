package com.just.carrental;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.just.carrental.R;
import com.just.carrental.Config;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.HashMap;

public class createAccount extends AppCompatActivity implements View.OnClickListener  {
    TextInputLayout fName , lName ,mobile, email ,password ;
    Button signUp;

    private static final String TAG = "createAccount";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);


        init();
    }
    public void init(){
        fName = findViewById(R.id.txt_ffirstName);
        lName = findViewById(R.id.txt_flastName);
        email=findViewById(R.id.txt_femail);
        password=findViewById(R.id.txt_fpassword);
        mobile= findViewById(R.id.txt_fmobile);


        fName.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!fName.getEditText().getText().toString().equals("")){
                    fName.setErrorEnabled(false);
                }

            }
        });

        lName.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(!lName.getEditText().getText().toString().equals("")){
                    lName.setErrorEnabled(false);
                }
            }
        });

        email.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                    Config.isValidEmailAddress(email,"Email Not Correct");
            }
        });

        password.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                    Config.isMoreThan3(password ,"Password Not Correct");
            }
        });

            mobile.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String regex = ("^\\d{10}$");
                String error = "Mobile Number Not Correct";
                if (!mobile.getEditText().getText().toString().matches(regex)){
                    mobile.setErrorEnabled(true);
                    mobile.setError(error);

                }else{
                    mobile.setErrorEnabled(false);

                }

            }
        });


        signUp = findViewById(R.id.btn_signUp);
        signUp.setOnClickListener(this);
    }

        public void sendRequest() {
        Boolean flag = true;

        if(fName.getEditText().getText().toString().equals("")){
            fName.setErrorEnabled(true);
            fName.setError("Enter First Name");
            flag=false;
        }
        if(lName.getEditText().getText().toString().equals("")){
            lName.setErrorEnabled(true);
            lName.setError("Enter Last Name");
            flag=false;
        }

        if (!Config.isValidEmailAddress(email,"Email Not Correct")){
            flag = false;

        }
        if (!Config.isMoreThan3(password,"Password Not Correct")){
            flag = false;
        }
        if (mobile.getEditText().getText().toString().equals("") || mobile.getEditText().getText().toString().length()<10){
            mobile.setErrorEnabled(true);
            mobile.setError("Mobile Number Not Correct");
            flag=false;
        }


        if (flag){
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.USER, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e(TAG, "onResponse: "+response );
                    try {
                        JSONObject base = new JSONObject(response);
                        JSONObject result= base.getJSONObject("result");
                        String status = result.getString("status");
                        String message = result.getString("message");

                        if (status.equals("0")){
                            Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
                            email.setErrorEnabled(true);
                            email.setError(message);
                        }else{
                            Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                            intent.putExtra("email", email.getEditText().getText().toString());
                            finish();
                            startActivity(intent);

                        }

                    }catch (JSONException e){
                        e.printStackTrace();}


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "onErrorResponse: "+error );
                }
            }){
                @Nullable
                @Override

                protected Map<String, String> getParams() throws AuthFailureError {

                   Map map = new HashMap<String ,String>();

                   map.put("type","create_customer_account");
                   map.put("fname",fName.getEditText().getText().toString().trim());
                   map.put("lname",lName.getEditText().getText().toString().trim());
                   map.put("email",email.getEditText().getText().toString().trim());
                   map.put("password",password.getEditText().getText().toString().trim());
                   map.put("mobile",mobile.getEditText().getText().toString().trim());
                   return map;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }else {
            //Toast.makeText(this, "Please Enter valid Information", Toast.LENGTH_LONG).show();
        }



    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_signUp:
                sendRequest();
                break;
        }
    }

}
