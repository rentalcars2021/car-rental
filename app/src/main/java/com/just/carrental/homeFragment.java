package com.just.carrental;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.just.carrental.Adapter.OfficeAdapter;
import com.just.carrental.Model.Office;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class homeFragment extends Fragment {


    View v;
    RecyclerView list;
    ArrayList dataList;

    private static final String TAG = "homeFragment";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        v=  inflater.inflate(R.layout.fragment_home, container, false);

        init();




        return v;
    }




    public void init(){
        dataList = new ArrayList<Office>();

        list = v.findViewById(R.id.list);
        getData();




    }




    public void getData(){

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.USER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    Log.e(TAG, "onResponse: "+response );
                    JSONObject base = new JSONObject(response);
                    JSONObject result = base.getJSONObject("result");
                    String status = result.getString("status");
                    String message = result.getString("message");

                    if (status.equals("0")){
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    }else{

                        JSONArray data = base.getJSONArray("data");

                        for (int i = 0;i<data.length();i++){
                            JSONObject object = data.getJSONObject(i);
                            Office o = new Office();
                            o.setId(object.getString("id"));
                            o.setFname(object.getString("fname"));
                            o.setLname(object.getString("lname"));
                            o.setEmail(object.getString("email"));
                            o.setPhone(object.getString("mobile"));
                            dataList.add(o);
                        }

                        OfficeAdapter officeAdapter = new OfficeAdapter(((HomeActivity) getContext()), dataList);
                        list.setLayoutManager(new LinearLayoutManager(getContext()));
                        list.setAdapter(officeAdapter);




                    }




                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse: "+error );
            }
        }){
            @Nullable
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map map = new HashMap<String,String>();

                map.put("type","view_all_office");
                return map;
            }
        };


        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);

    }









}